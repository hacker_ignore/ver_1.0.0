/**
 * Created by nikhil.agrawal on 02-10-2015.
 */

/*global demoWeb,$q,App*/

demoWeb.factory('categoryService',[
    '$q',
    'httpService',
    function($q,httpService){
      'use strict';

        return{
            getCategoryList:function(){
                var defer = $q.defer(),
                    URL = App.Constants.baseURL + "api_get_category.php";
                httpService.getRequest(URL,function(response){
                      defer.resolve(response.data);
                },function(error){
                      defer.reject(error);
                });
                return defer.promise;
            },

            addCategory:function(data){
                var defer = $q.defer(),
                    URL = App.Constants.baseURL + "api_category_add.php";

                httpService.postRequest(URL,data,function (response) {
                    defer.resolve(response.data);
                }, function (error) {
                    defer.reject(error);
                });
                return defer.promise;
            },

            updateCategory:function(data){
                var defer = $q.defer(),
                    URL = App.Constants.baseURL + "api_category_update.php";

                httpService.postRequest(URL,data,function (response) {
                    defer.resolve(response.data);
                }, function (error) {
                    defer.reject(error);
                });
                return defer.promise;
            },

            deleteCategory:function(id){
                var defer = $q.defer(),
                    URL = App.Constants.baseURL + "api_category_delete.php?id="+id;
                httpService.getRequest(URL,function(response){
                    defer.resolve(response.data);
                },function(error){
                    defer.reject(error);
                });
                return defer.promise;
            }
        };
    }
]);
