/**
 * Created by nikhil.agrawal on 19-09-2015.
 */

demoWeb.factory('sharedService', function() {
    var savedData = {}
    function set(data) {
        savedData = data;
    }
    function get() {
        return savedData;
    }

    return {
        set: set,
        get: get
    }

});
