/**
 * Created by nikhil.agrawal on 02-10-2015.
 */

/*global demoWeb,App */

demoWeb.factory('subCategoryService',[
    '$q',
    'httpService',
    function($q,httpService){
        'use strict';
        return{

            addSubCategory:function(data){
                var defer = $q.defer(),
                    URL = App.Constants.baseURL + "api_subcategory_add.php";

                httpService.postRequest(URL,data,function (response) {
                    defer.resolve(response.data);
                }, function (error) {
                    defer.reject(error);
                });
                return defer.promise;
            },

            editSubCategory:function(data){
                var defer = $q.defer(),
                    URL = App.Constants.baseURL + "api_subcategory_update.php";

                httpService.postRequest(URL,data,function (response) {
                    defer.resolve(response.data);
                }, function (error) {
                    defer.reject(error);
                });
                return defer.promise;
            },

            getSubCategoriesList:function(){
                var defer = $q.defer(),
                    URL = App.Constants.baseURL + "api_get_subcategory.php";

                httpService.getRequest(URL, function (response) {
                    defer.resolve(response.data);
                }, function (error) {
                    defer.reject(error);
                });
                return defer.promise;
            },

            deleteSubCategory:function(id){
                var defer = $q.defer(),
                    URL = App.Constants.baseURL + "api_subcategory_delete.php?id="+id;

                httpService.getRequest(URL, function (response) {
                    defer.resolve(response.data);
                }, function (error) {
                    defer.reject(error);
                });
                return defer.promise;
            }
        };

    }]);
