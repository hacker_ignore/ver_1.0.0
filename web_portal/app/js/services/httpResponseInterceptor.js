/**
 * Ajax interceptor
 * @author Sandip Nirmal
 *
 * Intercepts all ajax request, and handles spinner functionality based on requests.
 * It also logs requests and responses.
 */
// Array for all XHR calls
var xhrCallStack = [];

// Jquery ajax send
$(document).ajaxSend(function(e, xhr, settings) {

   // settings.startTime = e.timeStamp;
    //show spinner
    showSpinner(settings.url);

    /*// Log all requests to console on debug mode
    if (config.debug) {
        helper.debugLog("Request started at : " + moment(settings.startTime).format("YYYY-MM-DD:hh:mm:ss"));
        helper.debugLog("Request URL: " + settings.url);
        helper.debugLog("Request Type: " + settings.type);
    }*/
}).ajaxComplete(function(e, xhr, settings) {
  //  var endTime = moment(e.timeStamp);

    /*// Log all responses to console on debug mode
    if (config.debug) {
        helper.debugLog("Request Completed at : " + endTime.format("YYYY-MM-DD:hh:mm:ss"));
        helper.debugLog("Request URL: " + settings.url);
        helper.debugLog("Request Completed in :" + endTime.diff(settings.startTime) + " millisecond");
        helper.debugLog("Ready State :" + xhr.readyState);
        helper.debugLog("statusText :" + xhr.statusText);
        helper.debugLog("Status Code :" + xhr.status);
    }*/
    // hide spinner
    hideSpinner(settings.url, xhr.status);
});

/**
 * @ngdoc method
 * @name httpResponseInterceptor:showSpinner
 * @methodOf httpResponseInterceptor
 * @param {string} url - xmlHttpRequest
 * @description displays spinner for xmlHttpRequest progress
 */
function showSpinner(url) {
    /*var showSpinner = true;
    for (var i in spinnerConfig) {
        (url.indexOf(spinnerConfig[i]) !== -1) && (showSpinner = false);
    }
    // Start spinner on request start
    showSpinner && xhrCallStack.push(url);
    // Show Spinner only if there is ajax request in progress
    (showSpinner && xhrCallStack.length > 0) && $("#spinner").css('display', 'block');*/

    xhrCallStack.push(url);
    xhrCallStack.length > 0 && $("#spinner").css('display', 'block');
}

/**
 * @ngdoc method
 * @name httpResponseInterceptor:hideSpinner
 * @methodOf httpResponseInterceptor
 * @param {string} url - xmlHttpRequest url
 * @param {Number} status - xhr status
 * @description hides spinner once xmlHttpRequest progress completes
 */
function hideSpinner(url, status) {
    /*var unauthorised = false;

    // Check for http response status and unauthorisedConfig
    if (status === 401) {
        unauthorised = true;
        //check for spinner configs
        for (var i in unauthorisedConfig) {
            (url.indexOf(unauthorisedConfig[i]) !== -1) && (unauthorised = false);
        }
    }

    // If unauthorised start login flow again
    if (unauthorised) {
        $("#spinner").css('display', 'none');
        xhrCallStack = [];
        helper.reLogin();
    } else {
        var hideSpinner = true;

        for (var i in spinnerConfig) {
            (url.indexOf(spinnerConfig[i]) !== -1) && (hideSpinner = false);
        }
        // Stop spinner on request complete
        hideSpinner && xhrCallStack.splice(xhrCallStack.indexOf(url), 1);
        // hide spinner if no request in progress
        (xhrCallStack.length === 0) && $("#spinner").css('display', 'none');
    }*/

   // if (status === 200) {
        $("#spinner").css('display', 'none');
    //}
}