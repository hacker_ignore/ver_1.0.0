/**
 * Created by nikhil.agrawal on 19-09-2015.
 */

/*global demoWeb,$*/

demoWeb.service('httpService',function(){
    'use strict';
    return{

        postRequest:function(URL,Data,successcallback,failurecallback){
            $.ajax({
                url: URL,
                type: "POST",
                data: Data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(response) {
                   successcallback(response);
                },
                error: function(error) {
                  failurecallback(error);
                }
            });
        },

        getRequest:function(URL,successcallback,failurecallback){
            $.ajax({
                url: URL,
                type: "GET",
                contentType: false,
                cache: false,
                processData:false,
                dataType : "json",
                success: function( response ) {
                   successcallback(response);
                },
                error: function( xhr, status, errorThrown ) {
                   failurecallback(errorThrown);
                }
            });
        }

    };

});
