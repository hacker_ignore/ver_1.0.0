/**
 * Created by nikhil.agrawal on 07-10-2015.
 */

/*global demoWeb,App*/

demoWeb.factory('shopService',[
    '$q',
    'httpService',
    function($q,httpService){
        'use strict';

        return{

            getAllShops:function(){
                var defer = $q.defer(),
                    URL = App.Constants.baseURL + "api_get_shops.php";
                httpService.getRequest(URL,function(response){
                    defer.resolve(response.data);
                },function(error){
                    defer.reject(error);
                });
                return defer.promise;
            },

            addShop:function(data){
                var defer = $q.defer(),
                    URL = App.Constants.baseURL + "api_shop_add.php";

                httpService.postRequest(URL,data,function (response) {
                    defer.resolve(response.data);
                }, function (error) {
                    defer.reject(error);
                });
                return defer.promise;
            },

            updateShop:function(data){
                var defer = $q.defer(),
                    URL = App.Constants.baseURL + "api_shop_update.php";

                httpService.postRequest(URL,data,function (response) {
                    defer.resolve(response.data);
                }, function (error) {
                    defer.reject(error);
                });
                return defer.promise;
            },

            deleteShop: function (id) {
                var defer = $q.defer(),
                    URL = App.Constants.baseURL + "api_shop_delete.php?id="+id;
                httpService.getRequest(URL,function(response){
                    defer.resolve(response.data);
                },function(error){
                    defer.reject(error);
                });
                return defer.promise;
            }
        };
    }
]);
