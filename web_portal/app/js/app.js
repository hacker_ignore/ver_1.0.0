
/**
 * @ngdoc object
 * @name  demoWeb.Module:demoWeb Module
 * @requires ngRoute
 * @description The ngRoute module provides routing and deep linking services and directives for angular apps.
 * @requires $routeProvider init Application
 * @description Set the global configuration for configuring routes $routeProvider.
 * Sets route definition that will be used on route change when no other route definition is matched.
 *
 */

var demoWeb = angular.module('demoWeb',['ngRoute']);

demoWeb.config(['$routeProvider',

	function($routeProvider){
		$routeProvider
			.when('/',{
				templateUrl  : 'views/subCategoryList.html',
                controller: 'subCategoryListCtrl'
			})
            .when('/addSubCategory',{
                templateUrl  : 'views/addSubCategory.html',
                controller: 'addSubCategoryCtrl'
            })
            .when('/editSubCategory',{
                templateUrl  : 'views/editSubCategory.html',
                controller: 'editSubCategoryCtrl'
            })
            .when('/categoryList',{
                templateUrl  : 'views/categoryList.html',
                controller: 'categoryListCtrl'
            })
            .when('/addCategory',{
                templateUrl  : 'views/addCategory.html',
                controller: 'addCategoryCtrl'
            })
            .when('/editCategory',{
                templateUrl  : 'views/editCategory.html',
                controller: 'editCategoryCtrl'
            })
            .when('/shopsList',{
                templateUrl  : 'views/shopList.html',
                controller: 'shopListCtrl'
            })
            .when('/addShop',{
                templateUrl  : 'views/addShop.html',
                controller: 'addShopCtrl'
            })
            .when('/editShop',{
                templateUrl  : 'views/editShop.html',
                controller: 'editShopCtrl'
            })
			.otherwise({
                redirectTo: '/'
            });    
	}

]);

