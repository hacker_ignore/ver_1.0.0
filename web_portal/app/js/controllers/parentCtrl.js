/**
 * Created by nikhil.agrawal on 12-09-2015.
 */

demoWeb.controller('parentCtrl',function($rootScope,$scope,$location){

    $rootScope.currHoverMenu = "";

    $rootScope.menuClick = function(event){
          $(".active").removeClass("active");
          $(event.currentTarget).addClass("active");
    };

    $rootScope.showDropDown = function(id){
        if(id !== $rootScope.currHoverMenu){
             $(".menu-dropdown").css("display","none");
             $("#"+id).css("display","block");
        }
    };

    $rootScope.hideDropdown = function(){
        $(".menu-dropdown").css("display","none");
    };

    $rootScope.changeRoute = function (route) {
        $(".menu-dropdown").css("display","none");
        $location.path(route);
    };



});
