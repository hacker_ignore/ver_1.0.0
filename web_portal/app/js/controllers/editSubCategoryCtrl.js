/**
 * Created by nikhil.agrawal on 19-09-2015.
 */

/*global demoWeb,$,helper,FormData,alert */

demoWeb.controller('editSubCategoryCtrl',function($scope,$location,sharedService,subCategoryService){
    'use strict';

    $scope.subCategory = sharedService.get();
    $scope.browseButtonClick = function(){
        $('#imageBrowseHiddenButton').click();
    };

    $scope.updateSubCategory = function(){
        $('#editSubCategoryForm').submit();
    };

    $('#editSubCategoryForm').on('submit',function(e){
        e.preventDefault();
        subCategoryService.editSubCategory(new FormData(this)).then(function(){
            $location.path('/');
            helper.applyChanges($scope);
        },function(error){
            alert("ERROR : " + error);
        });
    });
});
