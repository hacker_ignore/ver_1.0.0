/**
 * Created by nikhil.agrawal on 19-09-2015.
 */

/*global demoWeb,$,App,helper,FormData,alert */

demoWeb.controller('addCategoryCtrl',function($scope,$rootScope,subCategoryService,categoryService){
    'use strict';
    $scope.subCategoryList = {};
    $scope.getSubCategoryList = function(){
        subCategoryService.getSubCategoriesList().then(function(response){
            $scope.subCategoryList = response;
            helper.applyChanges($scope);
        },function(error){
            alert("ERROR : " + error);
        });
    };

    $scope.getSubCategoryList();

    $scope.$on('$viewContentLoaded', function(){
        //Here your view content is fully loaded !!
        $(".js-example-basic-multiple").select2();

        $('select').select2()
            .on("change", function(e) {
                // mostly used event, fired to the original element when the value changes
                //alert(" Change Event");
                $('#cat_subcategories').val(e.val);
            });
    });

    $scope.browseButtonClick = function(){
        $('#imageBrowseHiddenButton').click();
    };

    $scope.addCategory = function(){
       $("#addCategoryForm").submit();
    };

    $("#addCategoryForm").on('submit',function(e){
        e.preventDefault();
        categoryService.addCategory(new FormData(this)).then(function(){
            $rootScope.changeRoute('/categoryList');
            helper.applyChanges($scope);
        },function(error){
            alert("ERROR : " + error);
        });
    });


});
