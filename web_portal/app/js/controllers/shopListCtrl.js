/**
 * Created by nikhil.agrawal on 06-10-2015.
 */

/*global demoWeb,helper,alert,$*/

demoWeb.controller('shopListCtrl',function($scope,$rootScope,shopService,sharedService){
    'use strict';
    $scope.shops = {};

    $scope.displayHoverStrip = function(i){
        $("#shop"+i).show();
    };

    $scope.hideHoverStrip = function(i) {
        $("#shop" + i).hide();
    };

    $scope.getAllShops = function(){
        shopService.getAllShops().then(function(response){
            $scope.shops = response;
            helper.applyChanges($scope);
        },function(error){
            alert(" ERROR : " + error);
        });
    };
    $scope.getAllShops();

    $scope.editShop = function(index){
        sharedService.set($scope.shops[index]);
        $rootScope.changeRoute('/editShop');
    };

    $scope.deleteShop = function(id){
        shopService.deleteShop(id).then(function(response){
            $scope.shops = response;
            helper.applyChanges($scope);
        },function(error){
            alert(" ERROR : " + error);
        });
    };
});
