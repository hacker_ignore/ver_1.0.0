/**
 * Created by nikhil.agrawal on 11-09-2015.
 */

/*global $,helper,App,demoWeb,alert*/

demoWeb.controller('subCategoryListCtrl',function($scope,$rootScope,sharedService,subCategoryService){
    'use strict';
    $scope.subCategoryList = {};

    $scope.displayHoverStrip = function(i){
        $("#subCategory"+i).show();
    };

    $scope.hideHoverStrip = function(i){
        $("#subCategory"+i).hide();
    };

    $scope.getSubCategoryList = function(){
        subCategoryService.getSubCategoriesList().then(function(response){
            $scope.subCategoryList = response;
            helper.applyChanges($scope);
        },function(error){
            alert("ERROR : " + error);
        });
    };

    $scope.deleteSubCategory = function(id){
        subCategoryService.deleteSubCategory(id).then(function(response){
            $scope.subCategoryList = response;
            helper.applyChanges($scope);
        },function(error){
            alert("ERROR : " + error);
        });
    };

    $scope.editSubCategory = function(index){
        sharedService.set($scope.subCategoryList[index]);
        $rootScope.changeRoute('/editSubCategory');

    };

    $scope.getSubCategoryList();

});