/**
 * Created by nikhil.agrawal on 17-09-2015.
 */

/*global demoWeb,helper,alert,$ */

demoWeb.controller('categoryListCtrl',function($scope,$rootScope,sharedService,categoryService){
    'use strict';

    $scope.categories = {};

    $scope.displayHoverStrip = function(i){
        $("#category"+i).show();
    };

    $scope.hideHoverStrip = function(i){
        $("#category"+i).hide();
    };

    $scope.getCategoryList = function(){
        categoryService.getCategoryList().then(function(response){
            $scope.categories = response;
            helper.applyChanges($scope);
        },function(error){
            alert(" ERROR : " + error);
        });
    };

    $scope.deleteCategory = function(id){
        categoryService.deleteCategory(id).then(function(response){
            $scope.categories = response;
            helper.applyChanges($scope);
        },function(error){
            alert(" ERROR : " + error);
        });
    };

    $scope.editCategory = function(index){
        sharedService.set($scope.categories[index]);
        $rootScope.changeRoute('/editCategory');

    };

    $scope.getCategoryList();
});
