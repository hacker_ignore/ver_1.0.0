/**
 * Created by nikhil.agrawal on 08-10-2015.
 */

/*global demoWeb,helper,alert,setTimeout,FormData,$*/

demoWeb.controller('editShopCtrl', function ($rootScope,$scope,sharedService,shopService,subCategoryService) {
    'use strict';

    $scope.shop = sharedService.get();
    $scope.subCategoryList = {};

    $scope.getSubCategoryList = function(){
        subCategoryService.getSubCategoriesList().then(function(response){
            $scope.subCategoryList = response;
            helper.applyChanges($scope);

        },function(error){
            alert("ERROR : " + error);
        });
    };

    $scope.getSubCategoryList();

    $scope.$on('$viewContentLoaded', function(){
        //Here your view content is fully loaded !!
        $(".js-example-basic-multiple").select2();
        var array = $scope.shop.shop_subcategories.split(",");
        setTimeout(function(){$("select").select2("val",array);},1000);

        $('select').select2()
            .on("change", function(e) {
                // mostly used event, fired to the original element when the value changes
                $('#shop_subcategories').val(e.val);
            });
    });

    $scope.browseButtonClick = function(){
        $('#imageBrowseHiddenButton').click();
    };

    $scope.updateShop = function(){
        $("#editShopForm").submit();
    };

    $("#editShopForm").on('submit',function(e){
        e.preventDefault();
        shopService.updateShop(new FormData(this)).then(function(){
            $rootScope.changeRoute('/shopsList');
            helper.applyChanges($scope);
        },function(error){
            alert("ERROR : " + error);
        });
    });

});