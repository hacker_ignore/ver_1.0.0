/**
 * Created by nikhil.agrawal on 17-09-2015.
 */

/*global demoWeb,App,FormData,helper,alert,$ */
demoWeb.controller('addSubCategoryCtrl',function($scope,$location,subCategoryService){
    'use strict';
    $scope.browseButtonClick = function(){
        $('#imageBrowseHiddenButton').click();
    };

    $scope.addSubCategory = function(){
        $('#addSubCategoryForm').submit();
    };

    $('#addSubCategoryForm').on('submit',function(e){
        e.preventDefault();
        subCategoryService.addSubCategory(new FormData(this)).then(function(){
                $location.path('/');
                helper.applyChanges($scope);
            },function(error){
                alert("ERROR : " + error);
        });
    });

});
