/**
 * Created by nikhil.agrawal on 04-10-2015.
 */

/*global demoWeb,$,App,helper,FormData,alert,setTimeout */

demoWeb.controller('editCategoryCtrl',function($scope,$rootScope,sharedService,subCategoryService,categoryService){
    'use strict';

    $scope.category = sharedService.get();
    $scope.subCategoryList = {};

    $scope.getSubCategoryList = function(){
        subCategoryService.getSubCategoriesList().then(function(response){
            $scope.subCategoryList = response;
            helper.applyChanges($scope);

        },function(error){
            alert("ERROR : " + error);
        });
    };

    $scope.getSubCategoryList();

    $scope.$on('$viewContentLoaded', function(){
        //Here your view content is fully loaded !!
        $(".js-example-basic-multiple").select2();
        var array = $scope.category.cat_subcategories.split(",");
        setTimeout(function(){$("select").select2("val",array);},1000);

        $('select').select2()
            .on("change", function(e) {
                // mostly used event, fired to the original element when the value changes
                $('#cat_subcategories').val(e.val);
            });
    });

    $scope.browseButtonClick = function(){
        $('#imageBrowseHiddenButton').click();
    };

    $scope.updateCategory = function(){
        $("#updateCategoryForm").submit();
    };

    $("#updateCategoryForm").on('submit',function(e){
        e.preventDefault();
        categoryService.updateCategory(new FormData(this)).then(function(){
            $rootScope.changeRoute('/categoryList');
            helper.applyChanges($scope);
        },function(error){
            alert("ERROR : " + error);
        });
    });
});

