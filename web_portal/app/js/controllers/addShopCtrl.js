/**
 * Created by nikhil.agrawal on 07-10-2015.
 */

/*global demoWeb,helper,alert,FormData,$*/

demoWeb.controller('addShopCtrl',function($scope,$rootScope,subCategoryService,shopService){
    'use strict';

    $scope.subCategoryList = {};

    $scope.browseButtonClick = function(){
        $('#imageBrowseHiddenButton').click();
    };

    $scope.getSubCategoryList = function(){
        subCategoryService.getSubCategoriesList().then(function(response){
            $scope.subCategoryList = response;
            helper.applyChanges($scope);
        },function(error){
            alert("ERROR : " + error);
        });
    };

    $scope.$on('$viewContentLoaded', function(){
        //Here your view content is fully loaded !!
        $(".js-example-basic-multiple").select2();

        $('select').select2()
            .on("change", function(e) {
                // mostly used event, fired to the original element when the value changes
                $('#shop_subcategories').val(e.val);
            });
    });

    $scope.addShop = function(){
        $("#addShopForm").submit();
    };

    $("#addShopForm").on('submit',function(e){
        e.preventDefault();
        shopService.addShop(new FormData(this)).then(function(){
            $rootScope.changeRoute('/shopsList');
            helper.applyChanges($scope);
        },function(error){
            alert("ERROR : " + error);
        });
    });

    $scope.getSubCategoryList();
});