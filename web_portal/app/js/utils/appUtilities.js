
/**
 * Created by sandip.nirmal on 08-09-2015.
 * appUtilities.js
 * Contains diiferent utility methods required in application
 */

var App = (function () {
    "use strict";

    /**
     * Creates new namespace with passed values and add it to Global App
     * @return {App}
     */
    function namespace() {
        var args = arguments,
            nsKey = args[0],
            nsValue = args[1],
            object = this,
            tokens = nsKey.split("."),
            len,
            i;

        if (typeof nsKey !== "string") {
            throw new Error("Manadatory namespace value must be string");
        }

        // strip redundant leading global
        if (tokens[0] === "App") {
            tokens = tokens.slice(1);
        }
        // length of arguments
        len = tokens.length;

        for (i = 0; i < len; i += 1) {
            // create a property if it doesn't exist
            if (object[tokens[i]] === undefined) {
                object[tokens[i]] = {};
            }
            object = object[tokens[i]];
        }

        if (nsValue !== undefined) {
            object = nsValue;
        }

        return object;
    }

    /**
     * Create a constant add it to namespace and assign specified value to it
     * @param prop - property name
     * @param value - property value
     */
    function createConstant(prop, value) {
        var constants = App.Constants || App.namespace("App.Constants");
        Object.defineProperty(constants, prop, {
            value: value,
            writable: false
        });
    }

    return {
        namespace: namespace,
        createConstant: createConstant
    };
}());
