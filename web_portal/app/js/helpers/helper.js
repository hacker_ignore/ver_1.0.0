/**
 * Created by nikhil.agrawal on 17-09-2015.
 */

var helper = {
    showSelectedImage : function(event,imgId){
        var selectedFile = event.target.files[0];
        var reader = new FileReader();
        var imgtag = document.getElementById(imgId);
        reader.onload = function(event) {
            imgtag.src = event.target.result;
        };
        reader.readAsDataURL(selectedFile);
    },

    applyChanges: function (scope) {
        if (!(scope.$$phase || (scope.$root && scope.$root.$$phase))) {
            scope.$apply();
        }
    },

    sendFormSubmitRequest : function(URL,data,successCallback,failureCallback){
        $.ajax({
            url: URL,        // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(data), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,
            success: function( json ) {
                // alert("success");
                //$rootScope.changeRoute("/");
                successCallback();
            },
            error: function( xhr, status, errorThrown ) {
                 failureCallback(xhr, status, errorThrown );
            },

            // Code to run regardless of success or failure
            complete: function( xhr, status ) {
            }
        });
    }

};
