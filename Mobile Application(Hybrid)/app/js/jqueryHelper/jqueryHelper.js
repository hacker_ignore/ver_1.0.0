/**
 * Created by nitin.hepat on 21-09-2015.
 */
$(document).ready(function(){
//    $('.button-collapse').sideNav({
//        menuWidth: 200, // Default is 240
//        edge: 'left', // Choose the horizontal origin
//        closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
//    });

    $('.title-div').flowtype({
        minFont   : 5,
        maxFont   : 40
    });
    $('body').flowtype({
        minimum   : 300,
        maximum   : 1200,
        minFont   : 11,
        maxFont   : 50,
        fontRatio : 30
    });



});