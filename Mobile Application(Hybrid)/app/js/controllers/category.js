/**
 * Created by nitin.hepat on 27-09-2015.
 */



retailorModule.controller('category',function($scope,mediatorService,$location){

    (function(){
        mediatorService.getCategoryDetails(function(data){
            $scope.categoryDetails = data;
            $scope.$apply();
        },function(error){
            $scope.noData = "";
        });
    })();

    $scope.navigateToUrl = function(id){
        $location.path('/subCategory/'+id);
    };


});
