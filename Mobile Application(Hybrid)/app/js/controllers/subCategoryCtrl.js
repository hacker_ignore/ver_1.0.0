/**
 * Created by nitin.hepat on 27-09-2015.
 */



retailorModule.controller('subCategoryCtrl',function($scope,mediatorService,$routeParams){


    /**
     *
     */
    (function(){
        mediatorService.getSubCategoryDetails($routeParams.id,function(data){
            $scope.subCategoryDetails = data;
            $scope.$apply();
        },function(error){
            $scope.noData = "";
        });
    })();


    $scope.images = ['courier','daily_needs','hotel','medical','movies','on_demand_services','repairs','resturant','taxi','travel']

});
