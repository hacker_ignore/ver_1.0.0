/**
 * Created by nitin.hepat on 21-09-2015.
 */


var retailorModule =  angular.module('retailorModule',['ngRoute']);
retailorModule.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
             .when('/category', {
                templateUrl: 'app/views/category.html',
                controller: 'category'
            }).when('/subCategory/:id', {
                templateUrl: 'app/views/subCategoryDetails.html',
                controller: 'subCategoryCtrl'
            }).when('/list/:id', {
                templateUrl: 'app/views/informationList.html',
                controller: 'listCtrl'
            })
                    .otherwise({
                redirectTo: '/category'


            });
    }
])