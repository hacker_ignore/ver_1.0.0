/**
 * Created by nitin.hepat on 27-09-2015.
 */


retailorModule.factory('mediatorService',function(httpService){
    var dataService = {};
    /**
     *
     * @param successCallback
     * @param failureCallBack
     */
    dataService.getCategoryDetails = function(successCallback,failureCallBack){
        httpService.getRequest('http://localretailers.in/app/api/api_get_category.php',function(data){
            successCallback && successCallback(data);
        },function(error){
            failureCallBack && failureCallBack(error);
        })
    };
    /**
     *
     * @param param
     * @param successCallback
     * @param failureCallBack
     */
    dataService.getSubCategoryDetails = function(param,successCallback,failureCallBack){
        var subCategoryUrl = "http://localretailers.in/app/api/api_category_details.php?cat_id="+param;
        httpService.getRequest(subCategoryUrl,function(data){
            successCallback && successCallback(data);
        },function(error){
            failureCallBack && failureCallBack(error);
        })
    };


    /**
     *
     * @param param
     * @param successCallback
     * @param failureCallBack
     */
    dataService.getShopList = function(param,successCallback,failureCallBack){
        var subCategoryUrl = "http://localretailers.in/app/api/api_shop_details.php?subcat_id="+param;
        httpService.getRequest(subCategoryUrl,function(data){
            successCallback && successCallback(data);
        },function(error){
            failureCallBack && failureCallBack(error);
        })
    };

    return dataService;
});
