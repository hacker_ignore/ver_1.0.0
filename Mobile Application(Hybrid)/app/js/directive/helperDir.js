/**
 * Created by nitin.hepat on 21-09-2015.
 */

retailorModule.directive('sideNav', function () {
    var template = '<div class="row side-menu" ng-init="toggle=true"><nav class="grey darken-4">' +
        '<div id="slide-out" class="side-nav">' +
        '<div class="waves-effect waves-teal" ng-repeat="x in navigationData"><a href="#!">{{x}}</a></div></div>' +
        '<div class="col s1"><a href="#" data-activates="slide-out" class=" button-collapse">' +
        '<i class="mdi-navigation-menu"></div></i></a>' +
        '<div class="header-wrapper">' +
        ' <div  ng-show="!toggle" class="input-field col s11">' +
        ' <div class="input-field col s10">'+
         '<input placeholder="search" id="first_name" type="search" class="validate">'+
        '</div><div class="col s1 search-close-img" ng-click="toggle=true">' +
        '<img style="height:30px;vertical-align: middle" src="app/images/quit.png"/></div>'+
        '</div>' +
        '<div class="search-div col s11"  ng-show="toggle" >' +
        '<div class="col s10"><span class="regular">Local Retailor</span></div>' +
        '<div class="col s1 search-wrapper">' +
        '<i id="searchIcon"class="medium material-icons"  ng-click="toggle=false">search</i>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</nav>' +
        '</div>';



    return{
//        templateUrl: 'app/views/header.html',
        template : template,
        scope: {
        navigationData: '='
        },

        link: function (scope) {
        scope.navigationData = ['Home', 'Channels', 'Playlist', 'Videos', 'Help', 'About Us'];
        }
        }

    });
